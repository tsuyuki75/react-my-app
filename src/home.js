import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {Button, Paper, Typography} from "@material-ui/core";
import { useHistory } from 'react-router-dom';

// スタイルの設定
const useStyles = makeStyles({
    conteiner: { height: '100vh' },
    content: { margin: '200px 150px' }
});
//初期表示画面用のコンポーネント
const Home = (props) => {
    const classes = useStyles();
    // react-routerからHistoryオブジェクトを受け取る
    const history = useHistory();
    return (
        <Paper className={classes.conteiner}>
            <div className={classes.content}>
                <Typography variant='button'>やることリストに</Typography>
                {/*ボタンクリック時にHistoryオブジェクトを使ってログイン画面に遷移*/}
                <Button variant='outlined' onClick={() => history.push('/login')}>
                    ログイン
                </Button>
            </div>
        </Paper>
    );
};
export default Home;